const path = require('path');
const CopyPlugin = require("copy-webpack-plugin");


function resolve(dir) {
  return path.join(__dirname, '..', dir)
}

module.exports = {
  context: resolve('src'),
  entry: './index.ts',
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: "../react", to: "react" },
      ],
    })
  ],
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
        exclude: /node_modules/
      }
    ],
  },
  resolve: {
    extensions: ['.ts', '.js'],
    // this is necessary for resolution of external libs like d3 in dev mode
    // when core is linked: webpack will take d3 from this node_modules but not from
    // internal so you get one version of d3
    modules: [path.resolve(__dirname, '../node_modules'), 'node_modules']
  },
  output: {
    filename: 'index.js',
    path: resolve('dist'),
    libraryTarget: 'umd',
    umdNamedDefine: true
  }
};
