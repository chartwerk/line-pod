import { SegmentSerie } from "../models/segment";
import { PodState } from "@chartwerk/core";
import { LineTimeSerie, LineOptions } from "../types";

import d3 from "d3";

export class Segments {
  // TODO: more semantic name
  private _d3Holder = null;

  constructor(private _series: SegmentSerie[], private _state: PodState<LineTimeSerie, LineOptions>) {
  }

  render(metricContainer: d3.Selection<SVGGElement, unknown, null, undefined>) {
    if(this._d3Holder !== null) {
      this._d3Holder.remove();
    }
    this._d3Holder = metricContainer.append('g').attr('class', 'markers-layer');
    for (const s of this._series) {
      this.renderSerie(s);
    }
  }

  protected renderSerie(serie: SegmentSerie) {
    serie.data.forEach((d) => {
      // @ts-ignore
      const startPositionX = this._state.xScale(d[0]) as number;
      // @ts-ignore
      const endPositionX = this._state.xScale(d[1]) as number;
      const width = endPositionX - startPositionX // Math.max(endPositionX - startPositionX, MIMIMUM_SEGMENT_WIDTH);
      
      this._d3Holder.append('rect')
        .attr('class', 'segment')
        .attr('x', startPositionX)
        .attr('y', 0)
        .attr('width', width)
        // @ts-ignore // TODO: remove ignore but boxParams are protected
        .attr('height', this._state.boxParams.height)
        .attr('opacity', 0.3)
        .style('fill', serie.color)
        .style('pointer-events', 'none');
    });

  }
}