import { MarkersConf, MarkerSerie } from "../models/marker";
import { PodState } from "@chartwerk/core";
import { LineTimeSerie, LineOptions } from "../types";

import d3 from "d3";

export class Markers {
  // TODO: more semantic name
  private _d3Holder = null;

  constructor(private _markerConf: MarkersConf, private _state: PodState<LineTimeSerie, LineOptions>) {

  }

  render(metricContainer: d3.Selection<SVGGElement, unknown, null, undefined>) {
    if(this._d3Holder !== null) {
      this._d3Holder.remove();
    }
    this._d3Holder = metricContainer.append('g').attr('class', 'markers-layer');
    for (const ms of this._markerConf.series) {
      this.renderSerie(ms);
    }
  }

  protected renderSerie(serie: MarkerSerie) {
    serie.data.forEach((d) => {
      let linePosition = this._state.xScale(d[0]) as number;
      this._d3Holder.append('line')
        .attr('class', 'gap-line')
        .attr('stroke', serie.color)
        .attr('stroke-width', '1px')
        .attr('stroke-opacity', '0.3')
        .attr('stroke-dasharray', '4')
        .attr('x1', linePosition)
        .attr('x2', linePosition)
        .attr('y1', 0)
        // @ts-ignore // TODO: remove ignore but boxParams are protected
        .attr('y2', this._state.boxParams.height)
        .attr('pointer-events', 'none');
      let circle = this._d3Holder.append('circle')
        .attr('class', 'gap-circle')
        .attr('stroke', serie.color)
        .attr('stroke-width', '2px')
        .attr('r', 4)
        .attr('cx', linePosition)
        .attr('cy', 5)

      if(this._markerConf !== undefined) {
        circle
          .attr('pointer-events', 'all')
          .style('cursor', 'pointer')
          .on('mousemove', () => this._markerConf.events.onMouseMove(d))
          .on('mouseout', () => this._markerConf.events.onMouseOut())
      }

    });

  }
}