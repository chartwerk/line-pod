export type MarkerElem = [number, any?];

export type MarkerSerie = {
  color: string;
  // TODO: make one-dimensional array with only x
  data: MarkerElem[] // [x, payload] payload is any data for tooltip
}

export type MarkersConf = {
  series: MarkerSerie[],
  events?: {
    onMouseMove?: (el: MarkerElem) => void;
    onMouseOut?: () => void;
  }
}
