export type SegmentSerie = {
  color: string;
  data: [number, number, any?][] // [from, to, payload?] payload is any data for tooltip
}