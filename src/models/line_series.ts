import { CoreSeries, yAxisOrientation } from '@chartwerk/core';
import { LineTimeSerie } from '../types';

import * as _ from 'lodash';


const LINE_SERIE_DEFAULTS = {
  maxLength: undefined,
  renderDots: false,
  renderLines: true,
  dashArray: '0',
  class: '',
  renderArea: false,
  yOrientation: yAxisOrientation.LEFT,
};

export class LineSeries extends CoreSeries<LineTimeSerie> {
  constructor(series: LineTimeSerie[]) {
    super(series, _.clone(LINE_SERIE_DEFAULTS));
  }
}
