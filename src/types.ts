import { Serie, Options } from '@chartwerk/core';

type LineTimeSerieParams = {
  maxLength: number,
  renderDots: boolean,
  renderLines: boolean, // TODO: refactor same as scatter-pod
  dashArray: string; // dasharray attr, only for lines
  class: string; // option to add custom class to each serie element
  renderArea: boolean; // TODO: move to render type
}

export type LineTimeSerie = Serie & Partial<LineTimeSerieParams>;
export type LineOptions = Options;

export type MouseObj = {
  x: number,
  y: number,
  xVal: number,
  yVal: number,
  series: { value: [number, number], color: string, label: string }[],
  chartX: number,
  chartWidth: number
}