# Chartwerk Line Pod
## Including

### Browser

#### Script tag
```html
<script src="https://unpkg.com/@chartwerk/line-pod@latest/dist/index.dev.js" type="text/javascript"></script>
```

#### Example
```html
<div id="line-chart" class="chart-block" style="width: 100%; height: 400px;"></div>

<script src="https://unpkg.com/@chartwerk/line-pod@latest/dist/index.dev.js" type="text/javascript"></script>
<script>
  new LinePod(document.getElementById('yourDivId', yourData, yourOptions)).render();
</script>
```

#### Other examples
[Static Chart](https://code.corpglory.net/chartwerk/line-pod/src/branch/main/examples/demo.html)

[Live Chart](https://code.corpglory.net/chartwerk/line-pod/src/branch/main/examples/demo_live.html)

[Vertical Chart](https://code.corpglory.net/chartwerk/line-pod/src/branch/main/examples/demo_vertical.html)


### Development

If you want to link `core` then clone it to same directory and then run

```
yarn link ../core
```

but don't add `resolutions` logic to `package.json`

then run

```
yarn install
yarn dev
```